//
//  FileManager.swift
//  MstApp
//
//  Created by OjiroTakahiro on 2014/11/17.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//
import UIKit

class FileManager: NSObject {
    
    // ドキュメントディレクトリのパスを返す
    func getDocumentPath() -> String {
        // ドキュメントディレクトリのパスを取得
        let documentPath = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
        // 返す
        return documentPath
    }

    // ダウンロードディレクトリのパスを返す
    func getDownloadPath() -> String {
        // インスタンス化
        let fm = FileManager()
        // ドキュメントディレクトリのパス
        let documentPath : String = fm.getDocumentPath()
        // ダウンロードディレクトリのパスの作成
        let downloadPath = documentPath + "/" + "Download"
        // 返す
        return downloadPath
    }
    
    // zipディレクトリのパスを返す
    func getZipPath() -> String {
        // インスタンス化
        let fm = FileManager()
        // ダウンロードディレクトリのパス
        let downloadPath : String = fm.getDownloadPath()
        // zipディレクトリのパスを作成
        let zipPath = downloadPath + "/" + "zip"
        // 返す
        return zipPath
    }
    
    // unzipディレクトリのパスを返す
    func getUnZipPath() -> String {
        // インスタンス化
        let fm = FileManager()
        // ダウンロードディレクトリのパス
        let downloadPath : String = fm.getDownloadPath()
        // zipディレクトリのパスを作成
        let unZipPath = downloadPath + "/" + "unzip"
        // 返す
        return unZipPath
    }
    
    // zipディレクトリ内のディレクトリ名一覧を取得
    func getZipDirectoryList() -> [String] {
        // インスタンス化
        let manage = NSFileManager.defaultManager()
        let fm = FileManager()
        // zipディレクトリのパスを取得
        let zipPath = fm.getZipPath()
        // zipディレクトリ内のディレクトリ名一覧を取得
        let zipList = manage.contentsOfDirectoryAtPath(zipPath, error: nil) as [String]
        // 返す
        return zipList
    }
    
    // zipディレクトリ内のディレクトリ数を取得
    func getZipDirectoryCnt() -> Int {
        // インスタンス化
        let fm = FileManager()
        // リスト取得
        let zipList = fm.getZipDirectoryList()
        // リスト件数を取得
        let zipCnt = zipList.count
        // 返す
        return zipCnt
    }

    // unzipディレクトリ内のディレクトリ名一覧を取得
    func getUnZipDirectoryList() -> [String] {
        // インスタンス化
        let manage = NSFileManager.defaultManager()
        let fm = FileManager()
        // zipディレクトリのパスを取得
        let unZipPath = fm.getUnZipPath()
        // zipディレクトリ内のディレクトリ名一覧を取得
        let unZipList = manage.contentsOfDirectoryAtPath(unZipPath, error: nil) as [String]
        // 返す
        return unZipList
    }
    
    // unzipディレクトリ内のディレクトリ数を取得
    func getUnZipDirectoryCnt() -> Int {
        // インスタンス化
        let fm = FileManager()
        // リスト取得
        let unZipList = fm.getZipDirectoryList()
        // リスト件数を取得
        let unZipCnt = unZipList.count
        // 返す
        return unZipCnt
    }

    func getBookName(bookIndex: Int) -> String {
        
        let fm = FileManager()
        
        let unZipListOne = fm.getUnZipDirectoryList()[bookIndex] as String
        
        let bookPath = fm.getUnZipPath() + "/" + unZipListOne + "/" + "sample"
        
        let manage = NSFileManager.defaultManager()
        
        let list = manage.contentsOfDirectoryAtPath(bookPath, error: nil) as [String]
        
        let str = list[1] as String
        
        return str
    
    }
    
}






