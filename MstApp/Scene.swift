//
//  Scene.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/12/08.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import MstIOSLibrary

public class Scene {
    private var _text = ""
    private var _image = ""
    private var _bgm = ""
    private var _se: [String] = []
    private var _voice: [String : String] = [:]
    
    public var text: String {
        return _text
    }
    
    public var image: String {
        return _image
    }
    
    public var bgm: String {
        return _bgm
    }
    
    private init() {
    }
    
    public class func load(json: JSON) -> Scene {
        let scene = Scene()
        scene._text = json["text"].stringValue
        scene._image = json["image"].stringValue
        scene._bgm = json["bgm"].stringValue
        scene._se = json["se"].arrayObject as [String]
        scene._voice = json["voice"].dictionaryObject as [String : String]
        return scene
    }
    
    public func voice(index: Int = 1) -> String? {
        if index < 1 || index > _voice.count {
            return ""
        }
        return _voice["\(index)"]
    }
}