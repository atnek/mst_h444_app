//
//  CollectionViewController.swift
//  MstApp
//
//  Created by OjiroTakahiro on 2014/11/11.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import UIKit
import MstIOSLibrary
import Alamofire

class CollectionViewController: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate {

    // プロパティ
    @IBOutlet weak var collectionView: UICollectionView!
    private var collection: [String] = []
    private let api = MstApi(host: Config.host)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // CollectionView用
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
       
        loadBookData()
        download()
    }

    private func loadBookData() {
        var error: NSError?
        collection = FileIO().cacheDir().cd("book")!.list(dir: "./", error: &error);
        println(collection)
    }
    
    private func download() {
        var bookInfo: [String : String]!
        api.bookList() {
            (info) in
            bookInfo = info
            for (bookId, bookName) in bookInfo {
                println(bookId)
                if !contains(self.collection, bookId){
                    self.api.downloadBook(bookId, completeHandler: nil)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // セクション数
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    // セクション内のセル数
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        println(collection.count)
        return collection.count
    }
    
    // セルを返す
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let bookId = collection[indexPath.row]
        let file = FileIO().cacheDir().cd(bookId)!
        
        var json: String?
        file.read("info.json", text: &json)
        println(json)
        var coverImage: NSData?
        file.read("cover.png", data: &coverImage)
        var cell : CollectionViewCellController = collectionView.dequeueReusableCellWithReuseIdentifier("SampleCell", forIndexPath: indexPath) as CollectionViewCellController
        cell.cellViewImage.image = UIImage(data: coverImage!)
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}








