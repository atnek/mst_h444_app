//
//  ZipDownloadViewController.swift
//  MstApp
//
//  Created by OjiroTakahiro on 2014/11/05.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import UIKit
import Alamofire

class ZipDownloadViewController: UIViewController {

    // プロパティ
    @IBOutlet weak var buttonDownload: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    // ダウンロードボタン押下メソッド
    @IBAction func downloadButtonMethod(sender: AnyObject) {
        NSLog("downloadClick")
        
        // インスタンス化
        let fm = FileManager()
        // ダウンロードディレクトリのパスを取得
        let downloadDirectoryPath = fm.getDownloadPath()
        // zipファイルのディレクトリパスを取得
        let zipDirectoryPath = fm.getZipPath()
        // unzipファイルのディレクトリパスを取得
        let unZipDirectoryPath = fm.getUnZipPath()
        
        // インスタンス化
        let manage = NSFileManager.defaultManager()
        
        // ダウンロードディレクトリが存在するかチェック
        if !manage.fileExistsAtPath(downloadDirectoryPath) {
            // ディレクトリを作成
            manage.createDirectoryAtPath(downloadDirectoryPath, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
        // zipディレクトリが存在するかチェック
        if !manage.fileExistsAtPath(zipDirectoryPath) {
            // ディレクトリを作成
            manage.createDirectoryAtPath(zipDirectoryPath, withIntermediateDirectories: true, attributes: nil, error: nil)
        }
        // unzipディレクトリが存在するかチェック
        if !manage.fileExistsAtPath(unZipDirectoryPath) {
            // ディレクトリを作成
            manage.createDirectoryAtPath(unZipDirectoryPath, withIntermediateDirectories: true, attributes: nil, error: nil)
        }

        // zipディレクトリの内容をリストで取得
        var zipList = fm.getZipDirectoryList()
        // リストの件数を取得
        var zipDirectoryCnt = fm.getZipDirectoryCnt()
        
        // 保存名
        var zipFileName = "zipFile" + String(zipDirectoryCnt) + ".zip"
        
        // zipディレクトリをNSURL型に変換
        let zipUrl = NSURL.fileURLWithPath(zipDirectoryPath)!
        
        // zipファイルのダウンロード
        Alamofire.download(.GET, "http://sourceforge.jp/frs/redir.php?m=iij&f=%2Fweb-service-api%2F45276%2Fsample.zip") { (temporaryURL, response) in
            // 保存先パスを返す
            return zipUrl.URLByAppendingPathComponent(zipFileName)
        } .progress { (bytesRead, totalBytesRead, totalBytesExpectedToRead) in
            // ダウンロードログ表示
            println(totalBytesRead)
        } .response { (request, response, _, error) in
        // ダウンロードが終わった後に実行される処理
            
            // レスポンスログ表示
            println(response)
            
            // unzip解凍先ディレクトリを作成  例) unzip/unzipfileXX/
            manage.createDirectoryAtPath(unZipDirectoryPath + "/" + "unZipFile" + String(zipDirectoryCnt),
                withIntermediateDirectories: true, attributes: nil, error: nil)
            
            // 解凍ファイルのパス 例) zip/zipFileXX.zip
            var zipPass = zipDirectoryPath + "/" + "zipFile" + String(zipDirectoryCnt) + ".zip"
            
            // 解凍先ディレクトリのパス 例) unzip/unzipFileXX/
            var unZipPass = unZipDirectoryPath + "/" + "unZipFile" + String(zipDirectoryCnt)
            
            // zip解凍
            SSZipArchive.unzipFileAtPath(zipPass, toDestination: unZipPass)
        }
        
        NSLog(downloadDirectoryPath)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
