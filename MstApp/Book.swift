//
//  Book.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/12/08.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import MstIOSLibrary

public class Book {
    private let bookId: String
    private let bookDir: String
    private let voicePlayer = AudioManager()
    private let bgmPlayer = AudioManager()
    private let sePlayer = AudioManager()
   
    private var currentBgmFile: String! = nil
    
    private var _scenes: [Int : Scene] = [:]
    
    private var _currentSceneNumber = 0
    
    public var onSendDisplayVoiceSignal: ((Int) -> Void)!
    
    public var sceneCount: Int {
        return _scenes.count
    }
    
    public var currentSceneNumber: Int {
        return  _currentSceneNumber
    }
    
    public var currentScene: Scene {
        return _scenes[_currentSceneNumber]!
    }
 
    public var currentBackgroundPath: String {
        return "\(bookDir)/image/\(currentScene.image)"
    }
    
    public init(bookId: String) {
        self.bookId = bookId
        bookDir = "\(CacheDirPath)/book/\(bookId)"
        NSLog("bookdir: %@", bookDir)
        
        // 仮実装
        sePlayer = AudioManager()
        sePlayer.loadFromBundle("page_sound", ext: "mp3")
        sePlayer.prepare()
    }
    
    public func ready() {
        var script: NSData?
        FileIO().read("\(bookDir)/script.json", data: &script)
        
        let json = JSON(data: script!)
        let count = json.count
        
        for i in 0..<count {
            _scenes[i] = Scene.load(json[i])
        }
    }
    
    public func prev() -> Bool {
        if _currentSceneNumber > 0 {
            voicePlayer.stop()
            _currentSceneNumber--
            return true
        }
        return false
    }
    
    public func next() -> Bool {
        if _currentSceneNumber + 1 < sceneCount {
            voicePlayer.stop()
            _currentSceneNumber++
            return true
        }
        return false
    }
    
    public func bgm() {
        let bgmFile = currentScene.bgm
        
        if bgmFile != currentBgmFile {
            currentBgmFile = bgmFile
            
            if bgmFile == "" {
                bgmPlayer.stop()
                return
            }
            
            var data: NSData?
            FileIO().read("\(bookDir)/bgm/\(bgmFile)", data: &data)
            bgmPlayer.stop()
            bgmPlayer.load(data!)
            bgmPlayer.volume = 0.05
            bgmPlayer.prepare()
            bgmPlayer.play()
        }
    }
    
    public func se() {
        sePlayer.play()
    }
    
    public func voice(index: Int = 1) {
        let voiceFile = currentScene.voice(index: index)
        if voiceFile == "" {
            voicePlayer.stop()
            return
        }
        if voiceFile == nil {
            // ディスプレイでボイス再生
            onSendDisplayVoiceSignal(index)
        } else if voiceFile != "" {
            let nextIndex = index + 1
            var data: NSData?
            FileIO().read("\(bookDir)/voice/\(voiceFile!)", data: &data)
            voicePlayer.onMediaEnded = {
                self.voice(index: nextIndex)
            }
            voicePlayer.load(data!)
            voicePlayer.volume = 1
            // FIXME: すぐ再生する
            voicePlayer.prepare()
            voicePlayer.play()
        }
    }
}