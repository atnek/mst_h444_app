//
//  MstWebSocketClient.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/10/13.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import MstIOSLibrary

class MstWebSocketClient: WebSocketConnector {
    
    override init(host: String, port: Int, secure: Bool = false) {
        super.init(host: host, port: port, secure: secure)
    }
    
    func call(method: String, params: String?, id: Int?) {
        send(toJson(method, params: params, id: id))
    }
    
    private func toJson(method: String, params: String?, id: Int?) -> String {
        var json = "{"
        json += "\"jsonrpc\" : \"2.0\""
        json += ", \"method\" : \"\(method)\""
        if params != nil {
            json += ", \"params\" : \(params!)"
        }
        if id != nil {
            json += ", \"id\" : \(id!)"
        }
        json += "}"
        NSLog(json)
        return json
    }
}