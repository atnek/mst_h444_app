//
//  Config.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/12/08.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation

public class Config {
    public class var host: String {
        return getUserDefaults("host")
    }
    
    public class var userId: String {
        return getUserDefaults("userid")
    }
    
    private class func getUserDefaults(key: String) -> String {
        return NSUserDefaults.standardUserDefaults().stringForKey(key)!
    }
}