//
//  ViewController.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/10/05.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire
import MstIOSLibrary

class ViewController: UIViewController, UIPageViewControllerDataSource{

    private var lastPageIndex: Int! = nil
    
    // プロパティ
    var pageViewController : UIPageViewController?
    private var contents: [Int: PageContentViewController] = [:]
    
    let userid = Config.userId
    
    private var currentBook: Book! = nil
    
    private var api: MstApi! = nil
    
    private var time = NSDate(timeIntervalSinceNow: 1.0)
    
    // websocketコネクタ
    let ws = MstWebSocketClient(host: Config.host, port: 9000)
    let bookId = "82f90e2e-c7a8-4b24-bddd-da5836b174f0"

    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("host: %@", Config.host)
        
        api = MstApi(host: Config.host)
        if !api.hasBook(bookId) {
            // 本をダウンロードしてない
            api.downloadBook(bookId) {
                (error) in
                if error == nil {
                    // ダウンロードが終わったら
                    self.initialize()
                } else {
                    println(error)
                }
            }
            return
        }
        initialize()
    }
    
    private func initialize() {
        currentBook = Book(bookId: bookId)
        currentBook.onSendDisplayVoiceSignal = sendDisplayVoiceSignal
        currentBook.ready()
        
        // pageviewcontrollerをインスタンス化
        pageViewController = storyboard!.instantiateViewControllerWithIdentifier("PageViewController") as UIPageViewController!
        pageViewController!.dataSource = self
        
        // 最初のページを登録
        pageViewController!.setViewControllers([createContentView()], direction: .Forward, animated: false, completion: nil)
        
        pageViewController!.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30)
        
        addChildViewController(pageViewController!)
        view.addSubview(pageViewController!.view)
        pageViewController!.didMoveToParentViewController(self)
        NSLog("connecting")
        // websocketサーバへ接続
        self.ws.onConnected = connected
        self.ws.onMessage = receivedMessage
        self.ws.connect()
        
        currentBook.bgm()
        currentBook.voice(index: 1)
    }
    
    // WebSocketのコネクションがはれたとき
    private func connected() {
        NSLog("connected")
        self.ws.call("handshake", params: "[\"\(self.userid)\", { \"kind\": \"app\" }]", id: 1)
    }
    
    // WebSocketでメッセージを受け取ったとき
    private func receivedMessage(message: String) {
        NSLog("%@", message)
        let json = JSON(data: message.dataUsingEncoding(NSUTF8StringEncoding)!)
        let result = json["result"]
        let messageId = result["messageId"].intValue
        
        NSLog("messageId: %d", messageId)
        
        switch messageId {
        case 1:
            break
        case 4:
            if result["to"].stringValue == "app" {
                currentBook.voice(index: result["no"].intValue)
            }
        case 5:
            ws.call("openBook", params: "[\"\(bookId)\"]", id: 1)
        default:
            break
        }
    }

    private func sendDisplayVoiceSignal(index: Int) {
        NSLog("sendSignal: %d", index)
        ws.call("relayvoice", params: "[\(index)]", id: 4)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // ContentViewをインスタンス化
    private func createContentView() -> PageContentViewController {
        let page = currentBook.currentSceneNumber
        if  contents[page] != nil {
            NSLog("use cache page: %d", page)
            return contents[page]!
        }
        NSLog("new")
        //pagecontentviewcontrollerをインスタンス化
        var pageContentViewController: PageContentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("PageContentViewController") as PageContentViewController
        
        // 画像パスを渡す
        pageContentViewController.imageFile = currentBook.currentBackgroundPath
        // ラベルテキストを渡す
        pageContentViewController.labelText = currentBook.currentScene.text
        // ページインデックスを渡す
        pageContentViewController.pageIndex = currentBook.currentSceneNumber
        // キャッシュ
        contents[page] = pageContentViewController
    
        // viewを返す
        return pageContentViewController
        
    }
   
    // ページを戻す
    func pageViewController(pageViewController:UIPageViewController, viewControllerBeforeViewController viewController:UIViewController) -> UIViewController? {
        if time.compare(NSDate()) == NSComparisonResult.OrderedDescending {
            return nil
        }
        if currentBook.prev() {
            updateTime()
            let index = currentBook.currentSceneNumber
            self.ws.call("pagesync", params: "[\(index)]", id: 2)
            currentBook.se()
            currentBook.bgm()
            currentBook.voice(index: 1)
            // viewを返す
            return createContentView()
        }
        return nil
    }
    // ページを進む
    func pageViewController(pageViewController:UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        if time.compare(NSDate()) == NSComparisonResult.OrderedDescending {
            return nil
        }
        if currentBook.next() {
            updateTime()
            let index = currentBook.currentSceneNumber
            self.ws.call("pagesync", params: "[\(index)]", id: 2)
            currentBook.se()
            currentBook.bgm()
            currentBook.voice(index: 1)
            // viewを返す
            return createContentView()
        }
        return nil
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return currentBook.sceneCount
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 1
    }
    
    private func updateTime() {
        time = NSDate(timeIntervalSinceNow: 2.0)
    }
}










