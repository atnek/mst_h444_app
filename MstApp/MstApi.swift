//
//  MstApi.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/12/02.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import Alamofire
import MstIOSLibrary

public class MstApi {
    
    private let bookRoot = "book"
    private let host: String
    private let port = 80
    private let version = 1
    
    public init(host: String) {
        self.host = host
    }
    
    public func hasBook(bookId: String) -> Bool {
        return FileIO().cacheDir().exists("\(bookRoot)/\(bookId)")
    }
    
    public func downloadBook(bookId: String, completeHandler: ((NSError?) -> Void)? = nil) {
        var bookDir = ""
        Alamofire.download(.GET, "http://\(host):\(port)/mst/api/v\(version)/book/\(bookId)") {
            (temporaryURL, response) in
            bookDir = "\(self.bookRoot)/\(bookId)"
            FileIO.shared.tempDir().create(bookDir, isDir: true)
            return NSURL.fileURLWithPath("\(bookDir)/app.zip")! // 保存先
        } .validate(statusCode: 200..<400)
        .progress {
            (bytesRead, totalBytesRead, totalBytesExpectedToRead) in
            let left = Double(totalBytesExpectedToRead) / Double(totalBytesRead)
            let progress = 100 - left
            NSLog("progress:%d", progress)
        } .response {
            (request, response, _, error) in
            
            // zipファイルダウンロード
            if error == nil {
                FileIO.shared.cacheDir().create(bookDir, isDir: true)
                SSZipArchive.unzipFileAtPath("\(TemporaryDirPath)/\(bookDir)/app.zip", toDestination: "\(CacheDirPath)/\(bookDir)")
            }
            if completeHandler != nil {
                completeHandler!(error)
            }
        }
    }
    
    public func bookList(completeHandler: (info: [String : String]) -> Void) {
        var infos: [String : String] = [:]
        Alamofire.request(.GET, "http://\(host):\(port)/mst/api/v\(version)/book").responseJSON {
            (_, _, json, error) in
            let data = json! as NSArray
            for info in data {
                let infometion = info as NSDictionary
                let bookId = info["bookId"]! as String
                let bookName = info["name"]! as String
                infos[bookId] = bookName
            }
            completeHandler(info: infos)
        }
    }
}