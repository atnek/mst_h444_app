//
//  PageContentViewController.swift
//  MstApp
//
//  Created by OjiroTakahiro on 2014/10/14.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import UIKit

class PageContentViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var labelText : String!
    var buttonText : String!
    var pageIndex : Int!
    var imageFile : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // 画像
        imageView.image = UIImage (named: imageFile)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
    }
    
    override func viewDidDisappear(animated: Bool) {
        /* 画像がたまると多分リーク起こす
        imageView.image = nil
        imageView.layer.sublayers = nil
        imageView = nil
        */
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
