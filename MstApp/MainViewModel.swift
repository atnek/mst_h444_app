//
//  MainViewModel.swift
//  MstApp
//
//  Created by 高橋 研太 on 2014/10/09.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation

public class MainViewModel {
    private var text: String = "";
    
    var Text: String {
        get {
            return text
        }
        set {
            text = newValue
        }
    }
}