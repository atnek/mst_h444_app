//
//  AudioManager.swift
//  MstIOSLibrary
//
//  Created by 高橋 研太 on 2014/10/27.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import AVFoundation

public class AudioManager: NSObject, AVAudioPlayerDelegate {
    private var player: AVAudioPlayer!
    
    public var onMediaEnded: (()->Void)?
    
    public var volume: Float {
        get {
            return player.volume
        }
        set {
            player.volume = newValue
        }
    }
    
    public class var shared: AudioManager {
        struct Singleton {
            static let instance = AudioManager()
        }
        return Singleton.instance
    }
    
    public func loadFromBundle(file: String, ext: String) -> NSError? {
        let path: String = NSBundle.mainBundle().pathForResource(file, ofType: ext)!
        var error: NSError?
        player = AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path), error: &error)
        player.delegate = self
        return error
    }
    
    public func load(data: NSData) -> NSError? {
        var error: NSError?
        player = AVAudioPlayer(data: data, error: &error)
        player.delegate = self
        return error
    }
    
    public func prepare() {
        player.prepareToPlay()
    }
    
    public func play() {
        if !player.playing {
            player.play()
        }
    }
    
    public func pause() {
        if player == nil {
            return
        }
        if player.playing {
            player.pause()
        }
    }
    
    public func stop() {
        player?.stop()
    }
    
    public func toggle() -> Bool {
        if player.playing {
            pause()
        } else {
            play()
        }
        return player.playing
    }
    
    private var step: Float = 0.0
    private var count: Float = 0
    public func fade(to: Float, ms: Int) {
        count = Float(ms) / 100.0
        step = (to - volume) / count
        NSLog("step: %f, count: %d, volume: %f", step, count, volume)
        let timer = NSTimer(timeInterval: 0.1, target: self, selector: "_fade:", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    private func _fade(timer: NSTimer) {
        volume += step
        if --count < 1 {
            timer.invalidate()
        }
        NSLog("step: %f, count: %d, volume: %f", step, count, volume)
    }
    
    public func audioPlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        onMediaEnded?()
    }
}