//
//  FileIO.swift
//  MstIOSLibrary
//
//  Created by 高橋 研太 on 2014/10/30.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation

public let TemporaryDirPath: String = NSTemporaryDirectory()

public let DocumentDirPath: String = {
    return NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as String
}()
    
public let CacheDirPath: String = {
    return NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] as String
}()
    
public let ApplicationSupportDirPath: String = {
    return NSSearchPathForDirectoriesInDomains(.ApplicationSupportDirectory, .UserDomainMask, true)[0] as String
}()

public class FileIO {
    
    private let manager: NSFileManager
    
    public class var shared: FileIO {
        struct Singleton {
            static let file = FileIO()
        }
        return Singleton.file
    }
    
    public init() {
        manager = NSFileManager()
        documentDir()
    }
    
    public func documentDir() -> FileIO {
        manager.changeCurrentDirectoryPath(DocumentDirPath)
        return self
    }
    
    public func tempDir() -> FileIO {
        manager.changeCurrentDirectoryPath(NSTemporaryDirectory())
        return self
    }
    
    public func cacheDir() -> FileIO {
        manager.changeCurrentDirectoryPath(CacheDirPath)
        return self
    }
    
    public func applicationSupportDir() -> FileIO {
        manager.changeCurrentDirectoryPath(ApplicationSupportDirPath)
        return self
    }
    
    public func cd(dir: String) -> FileIO? {
        if self.exists(dir) {
            manager.changeCurrentDirectoryPath(dir)
            NSLog("current: %@", manager.currentDirectoryPath)
            return self
        }
        return nil
    }
    
    public func pwd() -> String {
        return manager.currentDirectoryPath
    }
    
    public func exists(path: String) -> Bool {
        return manager.fileExistsAtPath(path)
    }
    
    public func list(dir: String = "./", inout error: NSError?) -> [String] {
        return manager.contentsOfDirectoryAtPath(getFullPath(dir), error: &error) as [String]
    }
    
    public func count(dir: String = "./", inout error: NSError?) -> Int {
        return list(dir: dir, error: &error).count
    }
    
    public func create(name: String, isDir: Bool = false) -> NSError? {
        var error: NSError? = nil
        if isDir {
            manager.createDirectoryAtPath(name, withIntermediateDirectories: true, attributes: nil, error: &error)
        } else {
            if manager.fileExistsAtPath(name) {
                let domain = ""
                let code = 10000
                let info = [NSLocalizedDescriptionKey: "\(name) already exists"]
            
                error = NSError(domain: domain, code: code, userInfo: info)
            } else {
                manager.createFileAtPath(name, contents: nil, attributes: nil)
            }
        }
        return error
    }
    
    public func mv (from: String, dist: String) -> NSError? {
        var error: NSError?
        manager.moveItemAtPath(from, toPath: dist, error: &error)
        return error
    }
    
    public func cp(from: String, dist: String) -> NSError? {
        var error: NSError?
        manager.copyItemAtPath(from, toPath: dist, error: &error)
        return error
    }
    
    public func write(file: String, text: String...) -> NSError? {
        let data = join("\n", text)
        let binary: NSData = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        return write(file, data: binary)
    }
    
    public func write(file: String, data: NSData) -> NSError? {
        create(file)
        
        var error: NSError? = nil
        let handle = NSFileHandle(forReadingAtPath: getFullPath(file))
        if handle == nil {
            let domain = ""
            let code = 10000
            let info = [NSLocalizedDescriptionKey: "could not open file"]
            
            error = NSError(domain: domain, code: code, userInfo: info)
        } else {
            handle!.writeData(data)
            handle!.synchronizeFile()
            handle!.closeFile()
        }
        return error
    }
    
    public func readLine(file: String, inout texts: [String]) -> NSError? {
        var text: String?
        var error = read(file, text: &text)
        if text != nil {
            texts = split(text!, { $0 == "\n"})
        } else {
            texts = []
        }
        return error
    }
    
    public func read(file: String, inout text: String?) -> NSError? {
        var data: NSData?
        var error = read(file, data: &data)
        if data == nil {
            text = nil
        } else {
            text = NSString(data: data!, encoding: NSUTF8StringEncoding)
        }
        return error
    }
    
    public func read(file: String, inout data: NSData?) -> NSError? {
        var error: NSError! = nil
        let handle = NSFileHandle(forReadingAtPath: getFullPath(file))
        if handle == nil {
            let domain = ""
            let code = 10000
            let info = [NSLocalizedDescriptionKey: "could not open file"]
            
            error = NSError(domain: domain, code: code, userInfo: info)
            data = nil
        } else {
            data = handle!.readDataToEndOfFile()
        }
        return error
    }
    
    public func delete(file: String) -> NSError? {
        var error: NSError? = nil
        manager.removeItemAtPath(file, error: &error)
        return error
    }
    
    private func getFullPath(path: String) -> String {
        if startsWith(path, "/") {
            return path
        }
        return "\(manager.currentDirectoryPath)/\(path)"
    }
}
