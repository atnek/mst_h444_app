//
//  WebSocketClient.swift
//  MstIOSLibrary
//
//  Created by 高橋 研太 on 2014/10/27.
//  Copyright (c) 2014年 高橋 研太. All rights reserved.
//

import Foundation
import Starscream

public class WebSocketConnector: WebsocketDelegate {
    private var socket: Websocket!
    
    public var onConnected: (() -> Void)?
    public var onDisconnected: ((NSError?) -> Void)?
    public var onError: ((NSError?) -> Void)?
    public var onMessage: ((String) -> Void)!
    public var onReceivedData: ((NSData) -> Void)!
    
    public var host: String
    public var port: Int
    public var secure: Bool
    
    public init(host: String, port: Int, secure: Bool = false) {
        self.host = host
        self.port = port
        self.secure = secure
    }
    
    public func connect(path: String = "/") {
        socket = Websocket(url: NSURL(scheme: secure ? "wss" : "ws", host: "\(host):\(port)", path: path)!)
        socket.delegate = self
        socket.connect()
    }
    
    public func disconnect() {
        socket.disconnect()
        socket.delegate = nil
        socket = nil
    }
    
    public func send(message: String) {
        socket.writeString(message)
    }
    
    public func send(data: NSData) {
        socket.writeData(data)
    }
    
    public func websocketDidConnect() {
        onConnected?()
    }
    
    public func websocketDidDisconnect(error: NSError?) {
        onDisconnected?(error)
    }
    
    public func websocketDidWriteError(error: NSError?) {
        onError?(error)
    }
    
    public func websocketDidReceiveMessage(text: String) {
        onMessage(text)
    }
    
    public func websocketDidReceiveData(data: NSData) {
        onReceivedData(data)
    }
}